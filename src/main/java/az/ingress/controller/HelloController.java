package az.ingress.controller;

import az.ingress.dto.UserDto;
import az.ingress.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class HelloController {
    private final UserService userService;

    @GetMapping("/public")
    public String publicHello(){
        return "Hello Public";
    }

    @GetMapping("/user")
    public String userHello(){
        return "user Hello";
    }

    @GetMapping("/admin")
    public String adminHello(){
        return "admin Hello";
    }

    @PostMapping("/register/user")
    public UserDto registerUser(@RequestBody UserDto userDto) {
        return userService.registerUser(userDto);
    }

    @PostMapping("/register/admin")
    public UserDto registerAdmin(@RequestBody UserDto userDto) {
        return userService.registerAdmin(userDto);
    }

}
