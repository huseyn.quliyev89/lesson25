package az.ingress.exception;

import lombok.Getter;

@Getter
public enum ErrorCodes {
    RESOURCE_NOT_FOUND("MS19-EXCEPTION-001"),
    BAD_REQUEST("MS19-EXCEPTION-002"),
    USERNAME_ALREADY_EXIST("MS19-EXCEPTION-003"),
    USERNAME_NOT_FOUND("MS19-EXCEPTION-004");


    final String code;

    ErrorCodes(String code) {
        this.code = code;
    }

}
