package az.ingress.service;

import az.ingress.dto.UserDto;
import az.ingress.enums.UserRole;

public interface UserService {
    UserDto save(UserDto userDto, UserRole roleName);
    UserDto registerUser(UserDto userDto);

    UserDto registerAdmin(UserDto userDto);

}
